# Javascript

- [async/await](https://jakearchibald.com/2017/await-vs-return-vs-return-await/)

- [Compilers and polyfills](https://tylermcginnis.com/compiling-polyfills/)

- [`this` keyword](http://javascriptissexy.com/understand-javascripts-this-with-clarity-and-master-it/)

- [Tree-shaking](https://webpack.js.org/guides/tree-shaking/)

- [serviceWorkers, webWorkers, webSockets](https://aarontgrogg.com/blog/2015/07/20/the-difference-between-service-workers-web-workers-and-websockets/)

- [Performance](https://medium.com/reloading/javascript-start-up-performance-69200f43b201)

- [Interviewing](https://medium.com/javascript-scene/10-interview-questions-every-javascript-developer-should-know-6fa6bdf5ad95)

- [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

## Typescript

- [Typescript tutorial](https://blog.logrocket.com/how-why-a-guide-to-using-typescript-with-react-fffb76c61614)

- [Understanding typescript function syntax](https://stackoverflow.com/questions/45485567/typescript-function-type-syntax-explanation)

- [Typescript React.SFC or React.PureComponent](https://medium.com/@nimelrian/be-careful-with-this-statement-463d3076d562)

- [Do's and Don'ts](https://www.typescriptlang.org/docs/handbook/declaration-files/do-s-and-don-ts.html)

- [Tagged union types](https://mariusschulz.com/blog/typescript-2-0-tagged-union-types)

- [Never type](https://mariusschulz.com/blog/typescript-2-0-the-never-type)

- [Freshness](https://basarat.gitbooks.io/typescript/docs/types/freshness.html)

- [Partial, Pick](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-1.html)

## Testing

- [Jest mocks](https://medium.com/@rickhanlonii/understanding-jest-mocks-f0046c68e53c)
- [Test pyramid](https://kentcdodds.com/blog/write-tests/)

## Others

- [AST](https://astexplorer.net/)
- [FE Handbook](https://frontendmasters.gitbooks.io/front-end-handbook-2017/content/learning/browsers.html)
