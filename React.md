# React

## Resources

- [React+typescript cheatsheet](https://github.com/sw-yx/react-typescript-cheatsheet)

- [React lifecycle](https://developmentarc.gitbooks.io/react-indepth/content/life_cycle/introduction.html)

## Blogs/Posts

- [React patterns](http://lucasmreis.github.io/blog/simple-react-patterns/)

- [Dumb/Smart components](https://medium.com/@dan_abramov/its-a-tradeoff-if-you-d-like-you-can-call-them-mixed-components-a7c3e1fedb2e)

- [Container components](https://reactpatterns.com/#container-component)

- [Analysing performance](https://medium.com/airbnb-engineering/recent-web-performance-fixes-on-airbnb-listing-pages-6cd8d93df6f4)

- [Function v Class components](https://overreacted.io/how-are-function-components-different-from-classes/)

- [Higher order components](https://medium.com/@franleplant/react-higher-order-components-in-depth-cf9032ee6c3e#.x63d7cgw7)

- [Higher order components gist](https://gist.github.com/sebmarkbage/ef0bf1f338a7182b6775)

- [React migration](https://medium.com/bumpers/isnt-our-code-just-the-best-f028a78f33a9)

- [Stateless functional components](https://reactpatterns.com/#stateless-function)

- [Render props](https://cdb.reacttraining.com/use-a-render-prop-50de598f11ce)

- [Render props 2](https://medium.com/@jrwebdev/react-render-props-in-typescript-b561b00bc67c)

- [Typescript with React/Redux](http://www.masteringreactjs.com/2018/04/23/making-sense-of-typescript-and-react-redux-connect/)

- [Understanding typescript+React errors](https://medium.com/innovation-and-technology/deciphering-typescripts-react-errors-8704cc9ef402)

- [Redux best practices](https://github.com/reduxjs/redux/issues/1171)

- [Redux performance](https://github.com/reduxjs/redux/issues/1751)

- [Handling side-effects in Redux](https://goshakkk.name/redux-side-effect-approaches/)
